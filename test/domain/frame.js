﻿define(['domain/frame'], function (frame) {

    describe('When frameNo is not 10', function () {

        it('finalFrame should be false', function () {
            var instance = new frame(1);
            expect(instance.finalFrame()).toBe(false);
            instance = new frame(2);
            expect(instance.finalFrame()).toBe(false);
            instance = new frame(3);
            expect(instance.finalFrame()).toBe(false);
            instance = new frame(4);
            expect(instance.finalFrame()).toBe(false);
            instance = new frame(5);
            expect(instance.finalFrame()).toBe(false);
            instance = new frame(6);
            expect(instance.finalFrame()).toBe(false);
            instance = new frame(7);
            expect(instance.finalFrame()).toBe(false);
            instance = new frame(8);
            expect(instance.finalFrame()).toBe(false);
            instance = new frame(9);
            expect(instance.finalFrame()).toBe(false);
        });
    });

    describe('When frameNo is 10', function () {

        it('finalFrame should be true', function () {
            var instance = new frame(10);
            expect(instance.finalFrame()).toBe(true);
        });
    });

    describe('When frame is not final frame', function () {

        it('noOfBowls should be 2', function () {
            var instance = new frame(1);
            expect(instance.noOfBowls()).toBe(2);
            instance = new frame(2);
            expect(instance.noOfBowls()).toBe(2);
            instance = new frame(3);
            expect(instance.noOfBowls()).toBe(2);
            instance = new frame(4);
            expect(instance.noOfBowls()).toBe(2);
            instance = new frame(5);
            expect(instance.noOfBowls()).toBe(2);
            instance = new frame(6);
            expect(instance.noOfBowls()).toBe(2);
            instance = new frame(7);
            expect(instance.noOfBowls()).toBe(2);
            instance = new frame(8);
            expect(instance.noOfBowls()).toBe(2);
            instance = new frame(9);
            expect(instance.noOfBowls()).toBe(2);
        });
    });

    describe('When frame is final frame', function () {

        it('noOfBowls should be 3', function () {
            var instance = new frame(10);
            expect(instance.noOfBowls()).toBe(3);
        });
    });

    describe('When bowl1 scores 10', function () {

        it('isStrike should be true', function () {
            var instance = new frame(1);
            instance.bowl1(10);
            expect(instance.isStrike()).toBe(true);
        });
    });

    describe('When bowl1 scores less than 10', function () {

        it('isStrike should be false', function () {
            var instance = new frame(1);
            instance.bowl1(9);
            expect(instance.isStrike()).toBe(false);
        });
    });

    describe('When bowl1 plus bowl2 scores 10', function () {

        it('isSpare should be true', function () {
            var instance = new frame(1);
            instance.bowl1(5);
            instance.bowl2(5);
            expect(instance.isSpare()).toBe(true);
        });
    });

    describe('When bowl1 plus bowl2 scores less than 10', function () {

        it('isSpare should be false', function () {
            var instance = new frame(1);
            instance.bowl1(4);
            instance.bowl2(4);
            expect(instance.isSpare()).toBe(false);
        });
    });

    describe('When finalFrame and isSpare', function () {

        it('thirdBallRequired should be true', function () {
            var instance = new frame(10);
            instance.bowl1(5);
            instance.bowl2(5);
            expect(instance.thirdBallRequired()).toBe(true);
        });
    });

    describe('When finalFrame and isStrike', function () {

        it('thirdBallRequired should be true', function () {
            var instance = new frame(10);
            instance.bowl1(10);
            expect(instance.thirdBallRequired()).toBe(true);
        });
    });

    describe('When finalFrame and not isStrike and not isSpare', function () {

        it('thirdBallRequired should be false', function () {
            var instance = new frame(10);
            instance.bowl1(1);
            instance.bowl2(1);
            expect(instance.thirdBallRequired()).toBe(false);
        });
    });

    describe('When not finalFrame and isStrike', function () {

        it('frameComplete should be true', function () {
            var instance = new frame(1);
            instance.bowl1(10);
            expect(instance.frameComplete()).toBe(true);
        });
    });

    describe('When not finalFrame and bowl2 not bowled', function () {

        it('frameComplete should be false', function () {
            var instance = new frame(1);
            instance.bowl1(1);
            expect(instance.frameComplete()).toBe(false);
        });
    });

    describe('When not finalFrame and bowl2 bowled', function () {

        it('frameComplete should be false', function () {
            var instance = new frame(1);
            instance.bowl1(1);
            expect(instance.frameComplete()).toBe(false);
        });
    });

    describe('When finalFrame and isStrike and bowl3 not bowled', function () {

        it('frameComplete should be false', function () {
            var instance = new frame(10);
            instance.bowl1(10);
            instance.bowl2(1);
            expect(instance.frameComplete()).toBe(false);
        });
    });

    describe('When finalFrame and isStrike and bowl3 bowled', function () {

        it('frameComplete should be false', function () {
            var instance = new frame(10);
            instance.bowl1(10);
            instance.bowl2(1);
            instance.bowl3(1);
            expect(instance.frameComplete()).toBe(true);
        });
    });

    describe('When finalFrame and isSpare and bowl3 not bowled', function () {

        it('frameComplete should be false', function () {
            var instance = new frame(10);
            instance.bowl1(5);
            instance.bowl2(5);
            expect(instance.frameComplete()).toBe(false);
        });
    });

    describe('When finalFrame and isSpare and bowl3 bowled', function () {

        it('frameComplete should be false', function () {
            var instance = new frame(10);
            instance.bowl1(5);
            instance.bowl2(5);
            instance.bowl3(1);
            expect(instance.frameComplete()).toBe(true);
        });
    });

    describe('When isStrike', function () {

        it('bowl1Text should be "X"', function () {
            var instance = new frame(1);
            instance.bowl1(10);
            expect(instance.bowl1Text()).toBe("X");
        });
    });

    describe('When not isStrike', function () {

        it('bowl1Text should be equal to the score', function () {
            var instance = new frame(1);
            instance.bowl1(3);
            expect(instance.bowl1Text()).toBe(3);
        });
    });

    describe('When isSpare', function () {

        it('bowl1Text should be equal to the score for bowl1 and bowl2Text should be "/"', function () {
            var instance = new frame(1);
            instance.bowl1(3);
            instance.bowl2(7);
            expect(instance.bowl1Text()).toBe(3);
            expect(instance.bowl2Text()).toBe("/");
        });
    });

    describe('When finalFrame and isSpare and bowl3 score is 10', function () {

        it('bowl3Text should be "X"', function () {
            var instance = new frame(1);
            instance.bowl1(3);
            instance.bowl2(7);
            instance.bowl3(10);
            expect(instance.bowl3Text()).toBe("X");
        });
    });

    describe('When finalFrame and isStrike and bowl2 and bowl3 score are 10', function () {

        it('bowl3Text should be "X"', function () {
            var instance = new frame(1);
            instance.bowl1(10);
            instance.bowl2(10);
            instance.bowl3(10);
            expect(instance.bowl3Text()).toBe("X");
        });
    });
});