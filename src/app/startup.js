define(['jquery', 'knockout', './router', 'bootstrap', 'knockout-projections'], function($, ko, router) {

  ko.components.register('home-page', { require: 'components/home-page/home' });

  ko.components.register('name-input', { require: 'components/name-input/name-input' });
    
  ko.components.register('score-card', { require: 'components/score-card/score-card' });

  ko.components.register('score-input', { require: 'components/score-input/score-input' });

  // [Scaffolded component registrations will be inserted here. To retain this feature, don't remove this comment.]

  // Start the application
  ko.applyBindings({ route: router.currentRoute });
});
