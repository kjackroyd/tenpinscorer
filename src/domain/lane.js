﻿define(["knockout", "player"], function (ko, player) {

    function Lane() {
        var self = this;

        self.currentPlayer = ko.observable();
        self.currentFrame = ko.observable();
        self.currentBowl = ko.observable();
        self.laneStarted = ko.observable(false);
        self.playerCount = ko.observable();
        self.players = ko.observableArray();
        self.playerCountEntered = ko.observable(false);

        self.complete = ko.pureComputed(function() {
            return self.currentFrame() > 10;
        });

        self.winner = ko.pureComputed(function () {
            var winner = self.players()[0];

            if (self.complete()) {
                ko.utils.arrayForEach(self.players(), function (plyr) {
                    var score = plyr.game.total();

                    if (score > winner.game.total()) {
                        winner = plyr;
                    }
                });
            }

            return winner;
        });

        self.gameOverMessage = ko.pureComputed(function() {
            return "Congratulations " + self.winner().playerName() + "! You have won with a score of " + self.winner().game.total();
        });

        self.start = function () {
            self.currentPlayer(self.players()[0]);
            self.currentFrame(1);
            self.currentBowl(1);
            self.laneStarted(true);
        };

        self.submitScore = function(score) {
            var frameComplete = self.currentPlayer().submitScore(self.currentFrame(), self.currentBowl(), score);

            if (frameComplete) {
                self.nextPlayer();
            } else {
                self.nextBowl();
            }
        };

        self.nextPlayer = function () {
            var playerNo = self.currentPlayer().playerNo;
            if (playerNo == self.players().length) {
                self.currentPlayer(self.players()[0]);
                self.currentFrame(1 + self.currentFrame());
            } else {
                //playerNo is not zero based so playerNo gives the next user in the array.
                self.currentPlayer(self.players()[playerNo]);
            }
            self.currentBowl(1);
        };

        self.nextBowl = function() {
            self.currentBowl(1 + self.currentBowl());
        };

        self.players.subscribe(function() {
            if (self.players().length == self.playerCount()) {
                self.start();
            }
        });
    }

    return Lane;

});