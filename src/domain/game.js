﻿define(["knockout", "frame"], function (ko, frame) {

    function Game() {
        var self = this;

        self.frames = ko.observableArray();

        self.total = ko.pureComputed(function() {
            return self.frames()[9].total();
        });

        self.init = function() {
            for (var i = 1; i <= 10; i++) {
                self.frames.push(new frame(i));
            }
        };

        self.submitScore = function (frameNo, bowlNo, score) {
            var currentFrame = self.frames()[frameNo - 1];

            switch (bowlNo) {
                case 1:
                    currentFrame.bowl1(score);
                    break;
                case 2:
                    currentFrame.bowl2(score);
                    break;
                case 3:
                    currentFrame.bowl3(score);
                    break;
                default:
                    alert("something has gone horribly wrong");
            }

            self.calculateTotals();

            return currentFrame.frameComplete();
        };

        self.calculateTotals = function () {
            var total = 0;

            for (var i = 0; i < 10; i++) {
                var currentFrame = self.frames()[i];

                if (currentFrame.hasTotal()) {
                    total = currentFrame.total();
                    continue;
                }

                if (currentFrame.frameComplete()) {
                    var frameTotal;

                    if (currentFrame.finalFrame()) {
                        frameTotal = self.getFinalFrameScore();
                    }
                    else if (currentFrame.isSpare()) {
                        frameTotal = self.getSpareScore(i);
                    }
                    else if (currentFrame.isStrike()) {
                        frameTotal = self.getStrikeScore(i);
                    } else {
                        frameTotal = currentFrame.bowl1() + currentFrame.bowl2();
                    }
                } else {
                    break;
                }

                if (frameTotal || frameTotal === 0) {
                    total += frameTotal;
                    currentFrame.total(total);
                    currentFrame.hasTotal(true);
                } else {
                    break;
                }
            }
        };

        self.getSpareScore = function(i) {
            var nextFrame = self.frames()[1 + i];

            if (nextFrame.bowl1() === 0 || nextFrame.bowl1() > 0) {
                return 10 + nextFrame.bowl1();
            }

            return undefined;
        };

        self.getStrikeScore = function (i) {
            var strikeTotal = 10;

            var nextFrame = self.frames()[1 + i];

            if (nextFrame.frameComplete()) {
                if (nextFrame.isStrike()) {
                    strikeTotal += 10;

                    if (nextFrame.finalFrame()) {
                        strikeTotal += nextFrame.bowl2();
                        return strikeTotal;
                    } else {
                        var thirdFrame = self.frames()[2 + i];

                        if (thirdFrame.bowl1() === 0 || thirdFrame.bowl1() > 0) {
                            strikeTotal += thirdFrame.bowl1();
                            return strikeTotal;
                        }
                    }
                }
                return strikeTotal + nextFrame.bowl1() + nextFrame.bowl2();
            }

            return undefined;
        };

        self.getFinalFrameScore = function() {
            var finalFrame = self.frames()[9];

            var frameTotal = finalFrame.bowl1() + finalFrame.bowl2();

            if (finalFrame.thirdBallRequired()) {
                frameTotal += finalFrame.bowl3();
            }

            return frameTotal;
        };

        self.init();
    }

    return Game;

});