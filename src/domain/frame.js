﻿define(["knockout"], function (ko) {

    function Frame(frameNo) {
        var self = this;

        self.frameNo = frameNo;
        self.bowl1 = ko.observable();
        self.bowl2 = ko.observable();
        self.bowl3 = ko.observable();
        self.hasTotal = ko.observable(false);
        self.total = ko.observable();

        self.finalFrame = ko.pureComputed(function() {
            return self.frameNo == 10;
        });

        self.noOfBowls = ko.pureComputed(function() {
            return self.finalFrame() ? 3 : 2;
        });

        self.isStrike = ko.pureComputed(function() {
            return self.bowl1() == 10;
        });

        self.isSpare = ko.pureComputed(function() {
            return self.bowl1() < 10 && self.bowl1()*1 + self.bowl2()*1 == 10;
        });

        self.thirdBallRequired = ko.computed(function() {
            return self.finalFrame() && (self.isStrike() || self.isSpare());
        });

        self.frameComplete = ko.pureComputed(function() {
            if (self.thirdBallRequired()) {
                return self.bowl3() === 0 || self.bowl3() > 0;
            } else {
                return self.isStrike() || self.bowl2() === 0 || self.bowl2() > 0;
            }
        });

        self.bowl1Text = ko.pureComputed(function() {
            return self.isStrike() ? "X" : self.bowl1();
        });

        self.bowl2Text = ko.pureComputed(function () {
            if (self.finalFrame()) {
                if (self.isStrike() && self.bowl2() == 10) {
                    return "X";
                }
            }

            if (self.isSpare()) {
                return "/";
            }

            return self.isStrike() && !self.finalFrame() ? "" : self.bowl2();
        });

        self.bowl3Text = ko.pureComputed(function () {
            if ((self.bowl2() == 10 || self.isSpare()) && self.bowl3() == 10) {
                return "X";
            }
            return self.isStrike() && (self.bowl2()*1 + self.bowl3()*1 == 10) ? "/" : self.bowl3();
        });
    }

    return Frame;

});