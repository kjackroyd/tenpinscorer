﻿define(["knockout", "game"], function (ko, game) {

    function Player(playerNo, playerName) {
        var self = this;

        self.playerNo = playerNo*1;
        self.playerName = ko.observable(playerName);

        self.game = new game();

        self.submitScore = function(frameNo, bowlNo, score) {
            return self.game.submitScore(frameNo, bowlNo, score);
        };
    }

    return Player;

});