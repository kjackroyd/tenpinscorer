define(["knockout", "text!./home.html", "lane"], function(ko, homeTemplate, lane) {

    function Home(route) {
        var self = this;

        self.lane = new lane();
        self.enterNoOfPlayers = ko.observable(true);
        self.playerCountEntered = ko.observable(false);

        self.showNameInput = ko.pureComputed(function() {
            return self.playerCountEntered() && self.lane.players().length < self.lane.playerCount();
        });

        self.showStart = ko.pureComputed(function () {
            return !self.lane.laneStarted() && self.lane.playerCount() > 0 && self.lane.players().length == self.lane.playerCount();
        });

        self.showScoreInput = ko.pureComputed(function() {
            return self.lane.laneStarted() && !self.lane.complete();
        });

        self.showGameOver = ko.pureComputed(function() {
            return self.lane.complete();
        });

        self.getPlayers = function() {
            self.playerCountEntered(true);
        };

        self.startMatch = function () {
            self.lane.start();
        };
    }

    return { viewModel: Home, template: homeTemplate };

});
