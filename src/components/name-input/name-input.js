define(['knockout', 'text!./name-input.html', "player"], function(ko, templateMarkup, player) {

    function NameInput(params) {
        var self = this;

        self.enterName = ko.observable(true);
        self.players = params.players;
        self.playerName = ko.observable();
        self.playerNo = ko.pureComputed(function() {
            return self.players().length + 1;
        });

        self.addPlayer = function() {
            self.players.push(new player(self.playerNo(), self.playerName()));
            self.playerName("");
            self.enterName(true);
        };
    }

    return { viewModel: NameInput, template: templateMarkup };
});
