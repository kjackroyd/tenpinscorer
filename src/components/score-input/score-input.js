define(['knockout', 'text!./score-input.html'], function(ko, templateMarkup) {

    function ScoreInput(params) {
        var self = this;

        self.lane = params.lane;
        self.player = self.lane.currentPlayer;
        self.frame = self.lane.currentFrame;
        self.bowl = self.lane.currentBowl;
        self.enterScore = ko.observable(true);
        self.score = ko.observable();
        self.error = ko.observable("");

        self.hasError = ko.pureComputed(function() {
            return !!self.error() ? "form-group has-error" : "form-group";
        });

        self.playerName = ko.computed(function() {
            return !!self.player ? self.player().playerName() : "";
        });

        self.remainingPins = ko.computed(function() {
            var currentFrame = self.player().game.frames()[self.frame() - 1];
            var previousBowl = 0;

            if (self.frame() == 10) {
                if (self.bowl() == 2) {
                    if (currentFrame.isStrike()) {
                        return 10;
                    }

                    previousBowl = currentFrame.bowl1();
                }

                if (self.bowl() == 3) {
                    if (currentFrame.isSpare() || (currentFrame.isStrike() && currentFrame.bowl2() == 10) ) {
                        return 10;
                    }

                    previousBowl = currentFrame.bowl2();
                }
            }
            else if (self.bowl() == 2) {
                previousBowl = currentFrame.bowl1();
            }

            return 10 - previousBowl;
        });

        self.addScore = function () {
            if (self.score() < 0) {
                self.error("Scores cannot be negative.");
            }
            else if (self.score() <= self.remainingPins()) {
                self.lane.submitScore(1 * self.score());
                self.score("");
                self.enterScore(true);
                self.error("");
            } else {
                self.error("There are only " + self.remainingPins() + " pins!");
            }
        };
    }

  // This runs when the component is torn down. Put here any logic necessary to clean up,
  // for example cancelling setTimeouts or disposing Knockout subscriptions/computeds.
  ScoreInput.prototype.dispose = function() { };
  
  return { viewModel: ScoreInput, template: templateMarkup };

});
