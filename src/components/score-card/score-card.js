define(['knockout', 'text!./score-card.html'], function(ko, templateMarkup) {

    function ScoreCard(params) {
        var self = this;

        self.player = params.player;

        self.playerNo = params.player.playerNo;
        self.playerName = params.player.playerName();
        self.game = params.player.game;
    }
  
  return { viewModel: ScoreCard, template: templateMarkup };

});
